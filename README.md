# Feature Flags

Simple Feature Flags implementation for Laravel applications. 
You can apply feature flag on:
- combinations of scopes (users/roles/organizations/whatever you need...),
- global (on/off).

## Installation

````shell
composer require trinetus/feature-flags
````

## Configuration

1. Migrate database (creates 1 new database table `feature_flags` for storing FF configs)

    ````shell
    php artisan migrate
    ````

2. Copy configuration file from vendor to your app.
    
    ````shell
    php artisan vendor:publish --provider="Trinetus\FeatureFlags\FeatureFlagsServiceProvider" --tag=config --force
    ````

3. Update your config as you need for your app - create your own checking scopes and pair it with eloquent models. 

    ````php
    'scopes' => [
        'users' => \App\Models\User::class,
        'roles' => \App\Models\Role::class,
        'organizations' => \App\Models\Organization::class,
    ]
    ````

4. Inject trait into specified models (**model have to be assigned to one of the scopes in config**). 

    ````php
    class YourModel extends Model {
        use \Trinetus\FeatureFlags\Traits\HasFeatureFlag;
        
        // ...
    }
    ````


## Code usage

### Using model trait methods

````php
$result = $modelInstance->hasFeatureFlag('my-new-feature');  
// returns bool

$result = $modelInstance->hasAllFeatureFlags(['my-feature','my-second-feature']);  
// returns bool (x AND y)

$result = $modelInstance->featureFlags();  
// returns array of all enabled FF
````

### Using model scope

````php
MyModel::withFeatureFlag('my-feature')->get(); 
// returns all MyModel instances which have enabled this feature flag

MyModel::withoutFeatureFlag('my-feature')->get(); 
// returns all MyModel instances which don't have enabled this feature flag
````

### Checking globally via service

````php
    use Trinetus\FeatureFlags\Services\FeatureFlagsService as FF;
    
    // ...
    
    if(!FF::active('my-feature')) {
        // true if FF is turned "on" globally (for all)
    }
    
    if(!FF::activeAny(['my-feature', 'my-second-feature'])) { 
        // true if at least one feature is turned "on" globally (for all)
    }
    
    if(!FF::activeAll(['my-feature', 'my-second-feature'])) {
        // true if all features are turned "on" globally (for all)
    }
````


## Managing (via console)

### List feature flags
````shell
php artisan ff:list
````

### Add new (interactive)
````shell
php artisan ff:add
````

### Edit existing (interactive)
````shell
php artisan ff:edit {id}
````

### Delete FF
````shell
php artisan ff:delete {id}
````

### Export whole config (json)
````shell
php artisan ff:export
````

### Import (and remove current) config in JSON format
````shell
php artisan ff:import {config}
````


## License

MIT License


## WIP (roadmap):

- model
- repository (crud operations)
- command for listing (into console) current feature flags
- command for adding/modifying/deleting feature flag by ID (interactive terminal)
- check service
- trait for specified model
- config ? (define user, group models  or define interface?)
- phpunit tests
- phpstan code quality checks (level 8)
- hierarchical checks (user.hasFF || user->group->hasFF)
