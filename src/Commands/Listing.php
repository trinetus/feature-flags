<?php

namespace Trinetus\FeatureFlags\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;

class Listing extends Command
{
    /**
     * @var string
     */
    protected $signature = 'ff:list';

    /**
     * @var string
     */
    protected $description = 'Feature Flags: list all feature flags';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $cfg = json_encode([ "users" => [10,20,34,45,95,182], "teams" => [87,36,28,12,39,47]]);
        $data = [
            ['new-feature', $this->formatConfig($cfg)],
            [new TableSeparator(), new TableSeparator()],
            ['another-feature', $this->formatConfig($cfg)]
        ];
        
        $this->customTableRender($data);
    }
    
    private function customTableRender(array $data): void
    {
        $this->newLine();
        
        (new Table($this->output))
            ->setHeaderTitle('Feature Flags')
            ->setHeaders(['Key', 'Config'])
            ->setRows($data)
            ->render();
        
        $this->newLine();
    }
    
    private function formatConfig(string $config): string
    {
        $arr = json_decode($config, associative: true);
        $out = [];
        
        foreach ($arr as $scope => $items) {
            $out[] = sprintf('%s: %s', $scope, implode(', ', $items));
        }
        
        return implode("\n", $out);
    }
}
