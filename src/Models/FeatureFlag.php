<?php

namespace Trinetus\FeatureFlags\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $key
 * @property array $config
 *
 */
class FeatureFlag extends Model
{
    protected $table = 'feature_flags';

    protected $fillable = [
        'key',
        'config',
    ];
    
    protected $casts = [
        'config' => 'array',
    ];
}
