<?php

namespace Trinetus\FeatureFlags\Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\Concerns\WithLaravelMigrations;
use Orchestra\Testbench\Concerns\WithWorkbench;
use Orchestra\Testbench\TestCase as TestbenchTestCase;

class TestCase extends TestbenchTestCase
{
    use WithWorkbench, RefreshDatabase, WithLaravelMigrations;

    protected function getPackageProviders($app)
    {
        return ['Trinetus\FeatureFlags\FeatureFlagsServiceProvider'];
    }

    /**
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $config = $app->get('config');

        $config->set('logging.default', 'errorlog');
        $config->set('database.default', 'testbench');
        $config->set('telescope.storage.database.connection', 'testbench');
        $config->set('queue.batching.database', 'testbench');

        $config->set('database.connections.testbench', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);
    }
}
