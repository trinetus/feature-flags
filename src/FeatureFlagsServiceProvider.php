<?php

namespace Trinetus\FeatureFlags;

use Illuminate\Support\ServiceProvider;
use Trinetus\FeatureFlags\Commands\Listing;

class FeatureFlagsServiceProvider extends ServiceProvider
{

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {            
            $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
            
            $this->commands([
                Listing::class,
            ]);
        }
    }

}
