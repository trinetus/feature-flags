<?php

namespace Trinetus\FeatureFlags\Tests\Models;

use Trinetus\FeatureFlags\Models\FeatureFlag;
use Trinetus\FeatureFlags\Tests\TestCase;

class FeatureFlagTest extends TestCase
{
    public function testSave()
    {
        // Create a new FeatureFlags instance
        $featureFlag = new FeatureFlag();

        // Set some attributes
        $featureFlag->key = 'my-feature';
        $featureFlag->config = ['enabled' => true];

        // Save the model
        $featureFlag->save();

        // Test that the model was saved correctly
        $this->assertDatabaseHas('feature_flags', [
            'key' => 'my-feature',
            'config' => json_encode(['enabled' => true]),
        ]);
    }
    
    public function testGetConfig()
    {
        // Create a new FeatureFlags instance
        $featureFlag = new FeatureFlag();

        // Set some attributes
        $featureFlag->key = 'my-feature';
        $featureFlag->config = ['enabled' => true];
        $featureFlag->save();

        // Test the getConfig method
        $config = FeatureFlag::find($featureFlag->id)?->config;
        $this->assertEquals(['enabled' => true], $config);
    }

    public function testDelete()
    {
        // Create a new FeatureFlags instance
        $featureFlag = new FeatureFlag();

        // Set some attributes
        $featureFlag->key = 'my-feature';
        $featureFlag->config = ['enabled' => true];

        // Save the model
        $featureFlag->save();

        // Delete the model
        $featureFlag->delete();

        // Test that the model was deleted correctly
        $this->assertDatabaseMissing('feature_flags', [
            'key' => 'my-feature',
        ]);
    }
}